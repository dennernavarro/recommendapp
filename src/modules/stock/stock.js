import client from 'api-client';

const stock = {
  namespaced: true,

  // module assets
  state: {
    prices: [],
  }, // module state is already nested and not affected by namespace option

  getters: {
    PRICES(state) {
      return state.prices;
    }, // -> getters['account/isAdmin']
  },
  mutations: {
    SET_SYMBOL_PRICES(state, payload) {
      state.prices = payload;
    }, // -> commit('account/login')
  },

  actions: {
    getStockPrice({ commit }, payload) {
      client.stockPriceGenerator(payload.stockSymbol, payload.timeWindow)
        .then((result) => {
          commit('SET_SYMBOL_PRICES', result);
        });
    }, // -> dispatch('stock/stockPrices')
  },
};

export default stock;
