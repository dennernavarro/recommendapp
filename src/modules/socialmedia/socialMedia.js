import client from 'api-client';

const socialMedia = {
  namespaced: true,

  // module assets
  state: {
    media: [],
    posts: [],
  }, // module state is already nested and not affected by namespace option

  getters: {
    MEDIA: state => state.media, // -> getters['socialMedia/MEDIAS']
    POSTS: state => state.posts, // -> getters['socialMedia/MEDIAS']
  },
  mutations: {
    SET_MEDIAS(state, payload) {
      state.media = payload;
    }, // -> commit('socialMedia/SET_MEDIAS')
    CHANGE_MEDIA(state, payload){
      const obj = state.media.find(media => payload.title === media.title)
      Object.assign(obj, payload);
    },
    SET_POSTS(state, payload) {
      state.posts = payload
    }
  },

  actions: {
    fetchSocialMedia({ commit }, payload) {
      return client
        .fetchSocialMedia(payload)
        .then(result => commit('SET_MEDIAS', result));
    }, // -> dispatch('socialMedia/fetchSocialMedia')
    selectMedia({commit}, payload) {
      commit('CHANGE_MEDIA', payload);
    },
    generatePosts({commit}, payload) {
      /** let's dispatch the event to generate social the count of media posts */
      client.socialMediaCountGenerator(payload.stockSymbol, payload.socialMedia)
        .then((result) => {
        });
    },
    selectSocialMedia({commit}, payload) {
      commit('SELECT_MEDIAS', payload);
    },
    topXpostsFromSocialMediaSelected({ commit }, payload) {
      console.log(payload.socialMediaList)
      return client
        .fetchSocialMediaPosts(payload.total, payload.socialMediaList)
        .then(result => commit('SET_POSTS', result));
    }, // -> dispatch('socialMedia/fetchSocialMedia')
  },
};

export default socialMedia;
