import client from 'api-client';

const recommendation = {
  namespaced: true,

  // module assets
  state: {
    recommendation: [],

  }, // module state is already nested and not affected by namespace option

  getters: {
    STOCK_RECOMMENDATION(state) {
      return state.recommendation;
    }, // -> getters['recommendation/STOCK_RECOMMENDATION']
  },
  mutations: {
    setRecommendation(state, payload) {
      state.recommendation = payload;
    }, // -> commit('setRecommendation')
  },

  actions: {
    fetchRecommendations({ commit }, payload) {
      return client
        .recommendationAlgorithm(payload)
        .then(result => commit('setRecommendation', result));
    }, // -> dispatch('recommendation/recommendationAlgorithm')
  },
};

export default recommendation;
