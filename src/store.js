import Vue from 'vue';
import Vuex from 'vuex';
import stock from './modules/stock/stock';
import socialMedia from './modules/socialmedia/socialMedia';
import recommendation from './modules/recommendation/recommendation';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    stock,
    recommendation,
    socialMedia,
  },
});
