import axios from 'axios';

export default {
  /**
   * Get all social media available
   * @returns {Promise<AxiosResponse<any> | never>}
   */
  fetchSocialMedia() {
    return axios
      .get(`${process.env.VUE_APP_API_URL}/social-media`)
      .then(response => response.data);
  },

  /**
   * Fetch the stock price for a given stock symbol
   * and period of time
   * @param symbol
   * @param dates
   * @returns {Promise<AxiosResponse<any> | never>}
   */
  stockPriceGenerator(symbol, dates) {
    return axios
      .get(`${process.env.VUE_APP_API_URL}/stock-price/${symbol}?dates=${dates}`)
      .then(response => response.data);
  },

  /**
   * Fetch Social media count
   * @param symbol
   * @param socialMediaType [Separate by comma]
   * @returns {Promise<AxiosResponse<any> | never>}
   */
  socialMediaCountGenerator(symbol, socialMediaType) {
    return axios
      .post(`${process.env.VUE_APP_API_URL}/social-media/${symbol}&media=${socialMediaType}`)
      .then(response => response.data);
  },

  /**
   * Payload requires:
   * - stockPrices and socialMediaCounts
   * @param payload
   * @returns {Promise<AxiosResponse<any> | never>}
   */
  recommendationAlgorithm(payload) {
    return axios
      .post(`${process.env.VUE_APP_API_URL}/social-media/`, { payload })
      .then(response => response.data);
  },

};
