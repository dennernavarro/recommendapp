import moment from 'moment';
import topPosts from './data/topPosts.json';

// Simulating API endpoints
/**
 * Fetching all Social Media available in the database
 * @param mockData
 * @param time
 * @returns {Promise<any>}
 */
const getSocialMedia = (mockData, time = 0) => new Promise((resolve) => {
  setTimeout(() => {
    resolve(mockData);
  }, time);
});

/**
 * Generate stock price with mock data
 * @param symbol STRING
 * @param dates INTEGER
 * @param time INTEGER
 * @returns {Promise<any>}
 */
const generateStockPrice = (symbol, dates, time = 0) => new Promise((resolve) => {
  setTimeout(() => {
    const arr = [];
    for (let i = 0; i < dates; i++) {
      const precision = 100; // 2 decimals
      const calc = Math.floor(Math.random() * (10 * precision - 1 * precision) + 1 * precision);
      const randomNum = calc / (1 * precision);
      const day = moment().subtract(i, 'days').format('l');
      let status = 'up';
      if (i !== 0) {
        status = arr[i - 1].price > randomNum ? 'down' : 'up';
      }
      const stock = {
        symbol,
        date: day,
        price: randomNum,
        status,
      };
      arr.push(stock);
    }
    resolve(arr);
  }, time);
});

/**
 * Generate count of posts for each given social media
 * @param symbol
 * @param socialMediaType
 * @returns {Promise<any>}
 */
const generateMediaPostsCount = (symbol, socialMediaType) => new Promise((resolve) => {
  // qtdy of posts up to 4.000
  const max = 4000;
  const min = 0;
  const total = socialMediaType.map(social => ({
    mediaType: social,
    totalPosts: Math.floor(Math.random() * (+max - +min) + +min),
  }));

  resolve(total);
});

/**
 * The recommendation consider
 * @param stockPrices
 * @param socialMediaCounts
 * @param time
 * @returns {Promise<any>}
 */
const fetchRecommendation = (payload, time = 0) => new Promise((resolve) => {
  setTimeout(() => {
    const prices = payload.prices.map(stock => stock.price);
    const pricesSum = prices.reduce((previous, current) => current += previous);
    let result = pricesSum / prices.length;

    if (payload.hasOwnProperty('risk') || payload.hasOwnProperty('ratio')) {
      const precision = 100; // 2 decimals
      const calc = Math.floor(Math.random() * (10 * precision - 1 * precision) + 1 * precision);
      result = calc / (1 * precision);
    }

    const data = {
      result,
    };
    resolve(data);
  }, time);
});

const generatePost = () => topPosts;

const fetchTopMediaPosts = (totalPosts, socialMediaList) => new Promise((resolve) => {
  const posts = socialMediaList.map((media) => {
    const mediaPost = {
      social_media: media,
      posts: [],
    };
    for (let i = 0; i < totalPosts; i++) {
      mediaPost.posts.push(generatePost());
    }
    return mediaPost;
  });

  setTimeout(() => {
    resolve(posts);
  }, 1000);
});
// END simulation

export {
  getSocialMedia,
  generateStockPrice,
  generateMediaPostsCount,
  fetchRecommendation,
  fetchTopMediaPosts,
};
