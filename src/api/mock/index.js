import socialMedia from './data/socialMedia.json';
import {
  getSocialMedia, generateMediaPostsCount,
  generateStockPrice, fetchRecommendation, fetchTopMediaPosts
} from './apiSimulator';

export default {
  /**
   * Get all social media available
   * @returns {Promise<AxiosResponse<any> | never>}
   */
  fetchSocialMedia() {
    return getSocialMedia(socialMedia, 1000); // wait 1s before returning social media
  },

  /**
   * Fetch the stock price for a given stock symbol
   * and period of time
   * @param symbol
   * @param dates
   * @returns {Promise<AxiosResponse<any> | never>}
   */
  stockPriceGenerator(symbol, dates) {
    return generateStockPrice(symbol, dates);
  },

  /**
   * Fetch Social media count
   * @param symbol
   * @param socialMediaType [Separate by comma]
   * @returns {Promise<AxiosResponse<any> | never>}
   */
  socialMediaCountGenerator(symbol, socialMediaType) {
    return generateMediaPostsCount(symbol, socialMediaType);
  },

  /**
   * Payload requires:
   * - stockPrices and socialMediaCounts
   * @param payload
   * @returns {Promise<any>}
   */
  recommendationAlgorithm(payload) {
    return fetchRecommendation(payload, 1000);
  },

  fetchSocialMediaPosts(totalPosts, socialMediaList) {
    return fetchTopMediaPosts(totalPosts, socialMediaList);
  },
};
