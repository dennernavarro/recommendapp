import Vue from 'vue';
import VeeValidate from 'vee-validate';
import App from './App.vue';
import store from './store';

Vue.use(VeeValidate);
Vue.config.productionTip = false;
Vue.filter('toUSD', (value) => {
  const newValue = value.toFixed(2);
  return `$${newValue}`;
});

new Vue({
  store,
  render: h => h(App),
}).$mount('#app');
