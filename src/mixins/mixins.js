import { mapGetters } from 'vuex';

const mixins = {
  computed: {
    ...mapGetters('socialMedia', {
      socialMediasAvailable: 'MEDIA',
    }),
    ...mapGetters('stock', {
      pricesPerDay: 'PRICES',
    }),
    ...mapGetters('recommendation', {
      recommendationObj: 'STOCK_RECOMMENDATION',
    }),
    ...mapGetters('socialMedia', {
      postsByMedia: 'POSTS',
    }),
    isAnySocialMediaSelected() {
      return this.mediaSelected.length;
    },

    mediaSelected() {
      return this.socialMediasAvailable.filter(social => social.selected);
    },
    mediaSelectedTitleList() {
      const selected = this.mediaSelected;
      if (selected.length) {
        return selected.map(media => media.title);
      };
      return [];
    },
  },
}

export default mixins;
